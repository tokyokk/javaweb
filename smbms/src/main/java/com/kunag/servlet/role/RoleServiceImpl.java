package com.kunag.servlet.role;

import com.kunag.dao.BaseDao;
import com.kunag.dao.role.RoleDao;
import com.kunag.dao.role.RoleDaoImpl;
import com.kunag.pojo.Role;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.Callable;

public class RoleServiceImpl implements RoleService{

    //引入Dao
    private RoleDao roleDao;

    public RoleServiceImpl() {
        roleDao = new RoleDaoImpl();
    }

    public List<Role> getRoleList() {

        Connection connection = null;
        List<Role> roleList = null;

        try {
            connection = BaseDao.getConnection();
            roleList = roleDao.getRoleList(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            BaseDao.closeResources(connection,null,null);
        }
        return roleList;
    }
}
