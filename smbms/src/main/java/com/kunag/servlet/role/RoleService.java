package com.kunag.servlet.role;

import com.kunag.pojo.Role;

import java.util.List;

public interface RoleService {

    //获取角色列表
    public List<Role> getRoleList() ;
}
