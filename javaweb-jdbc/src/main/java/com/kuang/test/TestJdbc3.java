package com.kuang.test;

import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class TestJdbc3 {

    @Test
    public void test()  {

        Connection connection = null;
        //配置信息
        //useUnicode=true&characterEncoding=utf-8   解决中文乱码
        String url = "jdbc:mysql://localhost:3306/jdbc?useUnicode=true&characterEncoding=utf-8";
        String username = "root";
        String password = "mysqlstart";

        //1.加载驱动
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

        //2.连接数据库，代表数据库
        connection = DriverManager.getConnection(url, username, password);

        //3.通知数据库开启事务，false 开启
        connection.setAutoCommit(false);

        String sql = "update account set money = money - 100 where name ='A'";
        connection.prepareStatement(sql).executeUpdate();

        String sql2 = "update account set money = money + 100 where name ='B'";
        connection.prepareStatement(sql2).executeUpdate();

        connection.commit();//以上两条SQL都执行成功了，就提交事务！
        System.out.println("sucess");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            connection.rollback();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
