package com.kuang.filter;

import javax.servlet.*;
import java.io.IOException;

public class CharacterEncodingFilter implements Filter {
    //初始化：web服务器启动，就已经初始化了，随时等待过滤对象出现！
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("CharacterEncodingFilter初始化");
    }

    //chain：链
    /*
    1.过滤器中的所有代码，在过滤特定请求的时候都会执行
    2.必须让过滤器继续同行
    chain.doFilter(request,response);
    */

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        response.setContentType("text-html;charset=utf-8");

        System.out.println("CharacterEncodingFilter执行前...");
        chain.doFilter(request,response);//让我们的程序继续走，如果不写，我们的程序到这里就停止了！
        System.out.println("CharacterEncodingFilter执行后...");
    }

    //销毁：web服务器关闭的时候，过滤就会销毁
    public void destroy() {
        System.out.println("CharacterEncodingFilter销毁");
    }
}
