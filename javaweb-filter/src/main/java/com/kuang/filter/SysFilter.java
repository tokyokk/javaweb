package com.kuang.filter;

import com.kuang.util.Constant;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;


public class SysFilter implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {

        //ServletRequest    HttpServletRequest
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        Object user_session = request.getSession().getAttribute(Constant.USER_SESSION);

        if (request.getSession().getAttribute(Constant.USER_SESSION)==null){
            ((HttpServletResponse) resp).sendRedirect("/error.jsp");
        }

        chain.doFilter(request,response);
    }

    public void destroy() {

    }
}
