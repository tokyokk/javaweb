package com.kuang.servlet;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

public class HelloServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

//        this.getInitParameter()   初始化参数
//        this.getServletConfig()   Servlet配置
//        this.getServletContext()  Servlet上下文
        ServletContext servletContext = this.getServletContext();

        String username = "qinjiang";//数据
        servletContext.setAttribute("username",username);//讲一个数据保存在ServletContext中，名字为username，值 username



//        PrintWriter writer = resp.getWriter();
//        writer.print("hello");
    }
}
